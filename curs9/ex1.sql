 CREATE DATABASE curs9;
 
 /* V1 */
 
 CREATE TABLE IF NOT EXISTS category(
	id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	name VARCHAR(100)
 );
 
 CREATE TABLE IF NOT EXISTS products(
	id INT(11)PRIMARY KEY NOT NULL AUTO_INCREMENT,
	name VARCHAR(100),
	category INT(11),
	price FLOAT(10,2),
	FOREIGN KEY (category) REFERENCES category(id)
 );
 
 
  
 /* V2 */
 
 CREATE TABLE products(
	id INT(11)PRIMARY KEY NOT NULL AUTO_INCREMENT,
	name VARCHAR(100),
	category INT(11),
	price FLOAT(10.2)
 );
 
 CREATE TABLE category(
	id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	name VARCHAR(100)
 );
 
 /* joins */
 
 SELECT products.id product_id, products.name product_name, products.price product_price, category.name product_category
 FROM products
 INNER JOIN category
 ON products.category = category.id;
 
 SELECT category.name, products.name
 FROM category
 JOIN products
 ON category.id = products.category;
 