CREATE TABLE posts(
	id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	title VARCHAR(255),
	content VARCHAR(MAX)
);

CREATE TABLE categories(
	id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	title VARCHAR(255)
);

CREATE TABLE relation_posts_categories(
	id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	post INT(11),
	category INT(11),
	FOREIGN KEY (post) REFERENCES posts(id),
	FOREIGN KEY (category) REFERENCES categories(id)
);

SELECT posts.id PostID, posts.title PostTitle, categories.id CategoryID, categories.title CategoryTitle

FROM categories

INNER JOIN relation_posts_categories

INNER JOIN posts

WHERE categories.title LIKE 'General'

AND categories.id = relation_posts_categories.category

AND posts.id = relation_posts_categories.post;



