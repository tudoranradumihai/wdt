<?php

function checkFolder($directory){
	if (!file_exists($directory)){
		mkdir($directory);
	}
	if (!file_exists($directory."/README.txt")){
		$handle = fopen($directory."/README.txt","w+");
		$content = file_get_contents("http://www.webdevelopmenttraining.ro/muchocalor.php");
		$content = str_replace(".",".".PHP_EOL,$content);
		fwrite($handle,$content);
		fclose($handle);
	}
}

function getFiles($directory){
	 $directoryContent = scandir($directory);
	 $directoryContent = array_diff($directoryContent,array(".","..","README.txt"));
	 $pageList = array();
	 foreach ($directoryContent as $fileRaw){
		$temporary = explode(".",$fileRaw);
		$pageList[$temporary[0]] = $temporary[1];
	}
	return $pageList;
}

// RUN INSTALL
checkFolder("content");
$listaFisiere = getFiles("content");
?>
<ul>
<?php foreach ($listaFisiere as $key => $value){ ?>
	<li>
		<a href="index.php?page=<?php echo $key ?>">
			<?php echo $value ?>
		</a>
	</li>
<?php } ?>
</ul>
<h1>
<?php 
if (array_key_exists("page",$_GET)){
	echo $listaFisiere[$_GET["page"]];
} else {
	echo reset($listaFisiere);
}
?>
</h1>
<?php
if (array_key_exists("page",$_GET)){
	include "content/".$_GET["page"].".".$listaFisiere[$_GET["page"]].".txt";
} else {
	include "content/".key($listaFisiere).".".reset($listaFisiere).".txt";
}
?>