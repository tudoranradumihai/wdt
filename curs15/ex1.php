<?php

Class A {
	private $var1;
	private $var2;
	private $var3;
	private $var4;
	private $var5;
	private $var6;
	private $var7;
	private $var8;
	private $var9;

	public function __set($name, $value){
		$this->$name = $value;
	}

	public function __get($name){

		return $this->$name;
	}

	public function __unset($name){
		unset($this->$name);
	}

	public function metodaDependentaDeHeroina(){
		echo "Op op heroina";
	}

	public static function independenceDay(){

	}

}
/*
$object = new A();
//$object->var = "Lorem Ipsum"; // NU MERGE
//$object->setVar1("Lorem Ipsum");
$object->var2 = "Lorem Lorem";
//$object->var3 = "Cluj-Napoca";
if (isset($object->var3)){
	echo "Oras: ".$object->var3;
}	
//unset($object->var3);
var_dump($object);
*/
A::independenceDay();