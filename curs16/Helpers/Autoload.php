<?php

function __autoload($className){
	$folders = array(
		"Controllers",
		"Models",
		"Helpers"
	);
	foreach ($folders as $folder){
		$filepath = $folder."/".$className.".php";
		if (file_exists($filepath)){
			require_once $filepath;
		}
	}
}