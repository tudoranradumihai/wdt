<?php

$names = array("Carina","Bianca","Raul","Oana","RazvanS","RazvanP","Erika","Cristina","Alina","Luca");

$fruits = array("mere","pere","prune");

$plm = array();

for ($i=0;$i<100;$i++){
	$j = rand(0,9);
	if (!array_key_exists($j,$plm)){
		$plm[$j] = 1;
	} else {
		$plm[$j] += 1;
	}
}


$sentences = array();
$numarTotal = 0;
for ($i=0;$i<10;$i++){
	$indexNames = rand(0,count($names)-1);

	$propozitie = $names[$indexNames]." are ".$plm[$i]." ".$fruits[rand(0,count($fruits)-1)];
	$sentences[] = $propozitie;
	
	unset($names[$indexNames]);
	$names = array_values($names);
	
	$numarTotal += $plm[$i];
}

echo implode("<br />",$sentences)."<br />";
echo "Numarul total de fructe este: osuta".$numarTotal;