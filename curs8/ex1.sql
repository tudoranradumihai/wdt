CREATE DATABASE curs8;

CREATE TABLE users (
	id INT(11),
    first_name VARCHAR(80),
    last_name VARCHAR(80),
    email_address VARCHAR(120)
);

INSERT INTO users (`id`,`first_name`) VALUES (1,'Ciuculucu');

DROP TABLE users;

DROP TABLE IF EXISTS users;

CREATE TABLE users (
	id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT UNIQUE,
    first_name VARCHAR(80),
    last_name VARCHAR(80),
    email_address VARCHAR(120)
);

INSERT INTO `users` (`first_name`,`last_name`) VALUES ('Radu','Tudoran');

SELECT * FROM users;

SELECT id, first_name FROM users;

SELECT id, first_name FROM users WHERE id=2;

SELECT id, first_name FROM users WHERE id=2 OR id=3;

SELECT id, first_name FROM users WHERE first_name LIKE 'Radu';

SELECT id, first_name FROM users WHERE first_name LIKE 'Radu';

SELECT id, first_name FROM users WHERE first_name LIKE 'Radu' AND id=3;

SELECT id, first_name FROM users WHERE first_name LIKE 'Radu' AND id>2;

SELECT id, first_name FROM users WHERE first_name LIKE 'R%';

UPDATE users 
SET email_address='luca.mudura@caine.rau'
WHERE id = 2;

/* GRESEALA DE NEREPETAT */
UPDATE users 
SET email_address='luca.mudura@caine.rau';

UPDATE users 
SET email_address='', last_name = 'Cicalaca'
WHERE first_name LIKE 'Radu';

DELETE FROM users 
WHERE email_address='luca.mudura@caine.rau';

ALTER TABLE users ADD telephone VARCHAR(10);

ALTER TABLE users ADD numar_de_telefon INT(11);

ALTER TABLE users MODIFY numar_de_telefon VARCHAR(10);

ALTER TABLE users DROP numar_de_telefon;


