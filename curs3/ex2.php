<?php

for ($i=0; $i<5; $i++){
	echo $i."<br />";
}

for ($i=5; $i>0; $i--){
	echo $i."<br />";
}

$users = array(
	0 => array(
		"nume" => "Ion",
		"locul_nasterii" => "Auschwitz"
	),
	1 => array(
		"nume" => "Valise",
		"locul_nasterii" => "NYC"
	),
	2 => array(
		"nume" => "John",
		"locul_nasterii" => "Ciorogarla"
	),
);

for ($i=0;$i<count($users);$i++){
	echo $users[$i]["locul_nasterii"]."<br />";
}
// asta e modul prin care for ii identic cu foreach

//echo count($users);