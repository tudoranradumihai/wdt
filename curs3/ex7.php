<?php

$content = file_get_contents("http://api.fixer.io/latest");
$array = json_decode($content,TRUE);

$a = array();
$b = array();
$c = array();
$d = array();

foreach ($array["rates"] as $key => $value){
	if (($value>0) && ($value<1)){
		$a[] = $key;
	} else if (($value>1) && ($value<2)){
		$b[] = $key;
	} else if (($value>2) && ($value<5)){
		$c[] = $key;
	} else {
		$d[] = $key;
	}
}

echo "Interval"."<br />";
echo "0-1: ".implode(", ",$a)."<br />";
echo "1-2: ".implode(", ",$b)."<br />";
echo "2-5: ".implode(", ",$c)."<br />";
echo "5-*: ".implode(", ",$d)."<br />";