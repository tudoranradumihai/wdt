<?php

$a = -5;
$a = abs($a); // http://php.net/manual/en/function.abs.php
//var_dump($a);

$b = 1.1;
$b = ceil($b); // http://php.net/manual/en/function.ceil.php
//var_dump($b);

$c = array(5,10,50);

//echo min($c); // http://php.net/manual/en/function.min.php
//echo max($c); // http://php.net/manual/en/function.max.php

$d = pi(); // pi ...
//echo $d;

$e = 3.250;
$e = round($e,1,PHP_ROUND_HALF_DOWN); // http://php.net/manual/en/function.round.php
var_dump($e);