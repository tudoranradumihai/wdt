<?php

function getSessionValue($key){
	if (array_key_exists("user", $_SESSION)){
		if (array_key_exists($key, $_SESSION["user"])){
			return $_SESSION["user"][$key];
		}
	}
}

$securityKey = $_SERVER["SERVER_ADDR"].".".time();
setcookie("key",md5($securityKey),time()+60);


if (array_key_exists("errors", $_SESSION)){
	echo "<ul>";
	foreach ($_SESSION["errors"] as $error) {
		echo "<li>".$error."</li>";
	}
	echo "</ul>";
}
?>
<form method="POST" action="management.php" >
	Firstname *:<br />
	<input type="text" name="firstname" placeholder="Firstname" value="<?php echo getSessionValue("firstname"); ?>" /><br />
	Lastname *:<br />
	<input type="text" name="lastname" placeholder="Lastname" value="<?php echo getSessionValue("lastname"); ?>"  /><br />
	CNP *:<br />
	<input type="text" name="cnp" placeholder="CNP" /><br />
	Address:<br />
	<input type="text" name="adress" placeholder="Address" /><br />
	ZIP:<br />
	<input type="text" name="zip" placeholder="ZIP" /><br />
	City:<br />
	<input type="text" name="city" placeholder="City" /><br />
	Country:<br />
	<input type="text" name="country" placeholder="Country" /><br />
	Email Address *:<br />
	<input type="text" name="email" placeholder="Email Address" /><br />
	Password *:<br />
	<input type="password" name="password" placeholder="" /><br />	
	Confirm Password *:<br />
	<input type="password" name="confirmpassword" placeholder="" /><br />
	<input type="submit" name="register" value="Register" />
</form>