<?php include "content/partials/header.php" ?>
<div class="content">
<?php
	if (array_key_exists("action", $_GET)){
		if (file_exists("content/".$_GET["action"].".php")){
			include "content/".$_GET["action"].".php";
		} else {
			echo "There is no file for the selected action";
		}
	}
?>
</div>