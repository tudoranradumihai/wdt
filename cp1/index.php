<?php

if (array_key_exists("action", $_GET)){
	if ($_GET["action"]=="list"){
		include "list.php";
	} else if ($_GET["action"]=="show"){
		include "show.php";
	} else {
		include "error.php";
	}
} else {
	include "list.php";
}