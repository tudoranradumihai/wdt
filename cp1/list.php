<?php

echo "<h1>LIST ARTICLES</h1>"."\n\t";

$articles = file_get_contents("http://www.webdevelopmenttraining.ro/json.php");
$articles = json_decode($articles,TRUE);
//var_Dump($articles);
$numberofarticles = 1;
if (array_key_exists("page",$_GET)){
	$page = intval($_GET["page"]);
} else {
	$page = 1;
}
$offset = ($page-1)*$numberofarticles+1;

echo "<div class=\"articles\">"."\n\t";
for ($i=$offset; $i<=($page*$numberofarticles); $i++){
		if (array_key_exists($i,$articles)){
			echo "<div class=\"article\">"."\n";
			echo "<h2>".$articles[$i]["title"]."</h2>"."\n";
			echo "<p>".$articles[$i]["description"]."</p>"."\n";
			echo "</div>"."\n";
		}
}
echo "</div>"."\n\t";

$numberofpages = ceil(count($articles)/$numberofarticles);
if ($numberofpages>1){
	for ($i=1;$i<=$numberofpages;$i++){
		echo "<a href=\"index.php?action=list&page=$i\">".$i."</a> ";
	}
}