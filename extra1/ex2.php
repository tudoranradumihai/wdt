<?php

$continutText = file_get_contents("http://www.webdevelopmenttraining.ro/muchocalor.php");
//var_dump($continutText);

function numaraCuvinte($text){
	$semneDePunctuatie = "`~!@#$%^&*()_+-=[]{}\|;:',<.>/?";
	$listaSemneDePunctuatie = array();
	for ($i=0; $i<strlen($semneDePunctuatie);$i++){
		$listaSemneDePunctuatie[] = $semneDePunctuatie[$i];
	}
	
	$text = strtolower($text);
	$text = str_replace($listaSemneDePunctuatie,"",$text);
	$listaCuvinte = explode(" ",$text);
	$listaContor = array();
	foreach ($listaCuvinte as $cuvant){
		if (array_key_exists($cuvant,$listaContor)){
			$listaContor[$cuvant]++;
		} else {
			$listaContor[$cuvant] = 1;
		}
	}
	return $listaContor;
}

$listaCuvintesiContor = numaraCuvinte($continutText);
var_dump($listaCuvintesiContor);