<?php

$variabila = "[ct] users (id [intpk],name [vc],price [f])";

function createQuery($query){
	$query = explode("(",$query);
	$headerQuery = str_replace("[ct]","CREATE TABLE",$query[0]);
	$contentQuery = explode(")",$query[1]);
	$contentQuery = explode(",",$contentQuery[0]);
	
	$newContentQuery = array();
	foreach ($contentQuery as $item){
		$item = str_replace("[int]","INT(11)",$item);
		$item = str_replace("[intpk]","INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT",$item);
		$item = str_replace("[f]","FLOAT(10,2)",$item);
		$item = str_replace("[vc]","VARCHAR(255)",$item);
		$newContentQuery[] = $item;
	}
	return $headerQuery." (\n".implode(",\n",$newContentQuery)."\n);";
}

echo createQuery($variabila);